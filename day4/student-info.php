<?php
if(isset($_GET['sid'])) {
    $sid=$_GET['sid'];
}
else {
    header("location: /php-training/day4/index.php?msg=nostudent");
}
require "class_lib.php";
$ln = new learn(); //Intialization of class
require "lang/eng.php";
require "class_msg.php";
$msg=new message();
$stin=$stfname=$stlname=$stemail=$stbio=$ststaus=$stc=$sts=$strollno='';//Multi variable creation
$sinfo=$ln->getsinglestudent($sid);//Getting the Single Student details
$stcse=$ln->getstudentclssec($sid); //Getting the Student Class details
if($sinfo) {
    $strollno=$sinfo['RollNo'];
    $stin=$sinfo['Initials'];
    $stfname=$sinfo['FirstName'];
    $stlname=$sinfo['LastName'];
    $stemail=$sinfo['Email'];
    $stbio=$sinfo['Bio'];
    $ststaus=$sinfo['Status'];
}
if($stcse) {
    $stc=$stcse['ClassID'];
    $sts=$stcse['SecID'];
}

    ?>
<html>
<head>
  <title>Day 4: Forms and CRUD Operations</title>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link href="/php-training/fontawesome/css/all.css" rel="stylesheet">
</head>
<body>
  <div class="container">
<h1><i class="fas fa-calendar-day"></i> Day 4: Forms and CRUD Operations</h1>
<h6>By Ram | git@gitlab.com:webstix-lab/php-training.git</h6>
<hr/>
   <div class="starter-template">

    <div class="row">
      <div class="col-8">
        <h4>Read and Update the Student Data</h4>
        <?php
        if(isset($_GET['msg'])) {
            if($_GET['msg']=='error') {
                $msg->alertdanger($inserterror);
            }
            if($_GET['msg']=='updated') {
                $msg->alertsuccess($stupdated);
            }
            if($_GET['msg']=='stuadded') {
                $msg->alertsuccess($stuadded);
            }
        }
            ?>
        <form action="actions/update-student.php" method="POST">
  <div class="form-row">
    <div class="col">
      <label for="siname">Student Initial:</label>
      <input type="text" required class="form-control" name="siname" value="<?php echo $stin;?>" placeholder="Initial">
    </div>
    <div class="col-7">
      <label for="sfname">Student First Name:</label>
      <input type="text" required class="form-control" name="sfname" value="<?php echo $stfname;?>" placeholder="First Name">
    </div>
    <div class="col">
      <div class="form-group">
        <label for="sfname">Student Last Name:</label>
      <input type="text" class="form-control" name="slname" value="<?php echo $stlname;?>" placeholder="Last Name">
    </div>
    </div>
  </div>

    <div class="form-row">
      <div class="col">
        <div class="form-group">
        <label for="srollno">Student Roll Number:</label>
        <input type="text" required class="form-control" name="srollno" value="<?php echo $strollno;?>" placeholder="Roll Number">
      </div>
      </div>
    </div>

    <div class="form-row">
      <div class="col">
        <div class="form-group">
        <label for="semail">Student Email:</label>
        <input type="email" required name="email" class="form-control" value="<?php echo $stemail;?>" placeholder="Email">
      </div>
      </div>
    </div>

    <div class="form-row">
      <div class="col">
        <div class="form-group">
        <label for="semail">Bio:</label>
      <textarea name="sbio" rows="4" cols="40" class="form-control" placeholder="Enter student bio"><?php echo $stbio;?></textarea>
      </div>
      </div>
    </div>

    <div class="form-row">
      <div class="col">
        <div class="form-group">
        <label for="semail">Class:</label>


        <select required class="form-control" id="sclass" name="sclass">
          <option  value="">-- Assign a class --</option>
            <?php
            $sclass=$ln->getallclass();

            for($sc=0; $sc<count($sclass); $sc++)
            {
                if($sclass[$sc]['ID']==$stc) { //Condition to check the selected value
                    $ck="selected='selected'";
                }
                else{
                    $ck='';
                }
                echo '<option value="'.$sclass[$sc]['ID'].'" '.$ck.' >'.$sclass[$sc]['Name'].'</option>';
            }
            ?>
        </select>
       </div>
      </div>
      <div class="col">
        <div class="form-group">
        <label for="semail">Sec:</label>
    <select required class="form-control" id="ssec" name="ssec">
      <option  value="">-- Assign a sec --</option>
            <?php
            $stsec=$ln->getallsec();

            for($e=0; $e<count($stsec); $e++)
            {
                if($stsec[$e]['ID']==$sts) {
                    $sck="selected='selected'";
                }
                else{
                    $sck='';
                }

                echo '<option value="'.$stsec[$e]['ID'].'" '.$sck.'>'.$stsec[$e]['SecName'].'</option>';
            }
            ?>

        </select>

      </div>
      </div>
    </div>

    <div class="form-row">
      <div class="col">
        <div class="form-group">
        <label for="semail">Status:</label>
    <select required class="form-control" id="sstatus" name="status">
      <option  value="">--Status --</option>
            <?php
            $status=$ln->getallstatus();
            for($te=0; $te<count($status); $te++)
            {
                if($status[$te]['Code']==$ststaus) {
                    $stsel="selected='selected'";
                }
                else {
                    $stsel='';
                }
                echo '<option value="'.$status[$te]['Code'].'" '.$stsel.'>'.$status[$te]['Name'].'</option>';
            }
            ?>
        </select>
      </div>
        <div class="form-group">
          <input type="hidden" name="stuid" value="<?php echo $sid;?>" >
    <button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Update</button>
      </div>
      </div>
    </div>


</form>







      </div>

    </div>




</div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</html>
