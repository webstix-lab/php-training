# Day 4: CRUD Operations (Create, Read, Update and Delete)
- Basic Form creation
- Browser side Validation (HTML)
- Server side Validation
- MySQL side Validation
### Create
- Insert form data in DB
- Insert with escape character
- Insert_id
### Read (Covered in Day 3)
- Display in the table
### Update
- How to do global update
- How to do only necessary update
### Delete
- Alert
- Soft Delete
- Hard Delete

## Try it on your own

Create action file and do the Create, update and delete

1. Create a form with these fields:

| Field Name     | Validation Type    | Field Type     |
| :------------- | :----------------- | :------------- |
| EmpCode        | Required           | Text           |
| Name           | Required           | Text           |
| Email          | Required, email format| Text        |
| Phone          | Required, only numeric| Text           |
| Designation    | Required              | Drop-Down |
| Skills         | Required              | Checkbox |
| Address        | Required              | Textarea |
| Salary         | Required              | Text, only numbers |


2. Insert all the info in the Employee table
3. Update email and skills of any employee of your choice
4. Update only the field that changes.
5. Create two button, one for soft delete and another for hard delete and confirm with the user before deleting it.
