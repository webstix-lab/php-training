<?php

require "class_lib.php";
$ln = new learn(); //Intialization of class
require "lang/eng.php";
require "class_msg.php";
$msg=new message();

    ?>
<html>
<head>
  <title>Day 4: Forms and CRUD Operations</title>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link href="/php-training/fontawesome/css/all.css" rel="stylesheet">
</head>
<body>
  <div class="container">
<h1><i class="fas fa-calendar-day"></i> Day 4: Forms and CRUD Operations</h1>
<h6>By Ram | git@gitlab.com:webstix-lab/php-training.git</h6>
<hr/>
   <div class="starter-template">

    <div class="row">
      <div class="col-8">
        <h4>Add New Student</h4>
        <?php
        if(isset($_GET['msg'])) {
            if($_GET['msg']=='error') {
                $msg->alertdanger($inserterror);
            }
            if($_GET['msg']=='nostudent') {
                $msg->alertdanger($nostudent);
            }
        }
            ?>
        <form action="actions/add-student.php" method="POST">
  <div class="form-row">
    <div class="col">
      <label for="siname">Student Initial:</label>
      <input type="text" required class="form-control" name="siname" placeholder="Initial">
    </div>
    <div class="col-7">
      <label for="sfname">Student First Name:</label>
      <input type="text" required class="form-control" name="sfname" placeholder="First Name">
    </div>
    <div class="col">
      <div class="form-group">
        <label for="sfname">Student Last Name:</label>
      <input type="text" class="form-control" name="slname" placeholder="Last Name">
    </div>
    </div>
  </div>

    <div class="form-row">
      <div class="col">
        <div class="form-group">
        <label for="srollno">Student Roll Number:</label>
        <input type="text" required class="form-control" name="srollno" placeholder="Roll Number">
      </div>
      </div>
    </div>

    <div class="form-row">
      <div class="col">
        <div class="form-group">
        <label for="semail">Student Email:</label>
        <input type="email" required name="email" class="form-control" placeholder="Email">
      </div>
      </div>
    </div>

    <div class="form-row">
      <div class="col">
        <div class="form-group">
        <label for="semail">Bio:</label>
      <textarea name="sbio" rows="4" cols="40" class="form-control" placeholder="Enter student bio"></textarea>
      </div>
      </div>
    </div>

    <div class="form-row">
      <div class="col">
        <div class="form-group">
        <label for="semail">Class:</label>


        <select required class="form-control" id="sclass" name="sclass">
          <option  value="">-- Assign a class --</option>
            <?php
            $sclass=$ln->getallclass();
            for($sc=0; $sc<count($sclass); $sc++)
            {
                echo '<option value="'.$sclass[$sc]['ID'].'">'.$sclass[$sc]['Name'].'</option>';
            }
            ?>
        </select>

      </div>
      </div>
      <div class="col">
        <div class="form-group">
        <label for="semail">Sec:</label>
    <select required class="form-control" id="ssec" name="ssec">
      <option  value="">-- Assign a sec --</option>
            <?php
            $stsec=$ln->getallsec();

            for($e=0; $e<count($stsec); $e++)
            {
                echo '<option value="'.$stsec[$e]['ID'].'">'.$stsec[$e]['SecName'].'</option>';
            }
            ?>

        </select>
      </div>
      </div>
    </div>

    <div class="form-row">
      <div class="col">
        <div class="form-group">
          <input type="hidden" name="status" value="A" />
    <button type="submit" class="btn btn-success"><i class="fas fa-user-plus"></i> Add New student</button>
      </div>
      </div>
    </div>


</form>







      </div>

    </div>




</div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</html>
