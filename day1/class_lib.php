<?php //This is called longtag preisouly this was decalred like this <? without the php and it is called as shot tag

class learn //A class name should be without any paranthesis
{
    function helloworld() //Basic function with just an echo statement
    {
        echo 'Hello World! Welcome to PHP Tutorial!';

    }

    function basicadd($v1, $v2)  //The params don't have to be the same varialbe as passing function. See I have used $v1 and $v2 where as in the working file you can see it is decalared as $a, $b
    {
        echo $v1 + $v2;

    }

    function addret($v1, $v2) //Function with return concept
    {
        $result=$v1 + $v2;
        return $result; //Return the result

    }

    function medadd($data) //Function with parameter as array Associative
    {
        echo $data['first'] + $data['second'];


    }

    function compadd($data) //Function with parameter as array Indexed
    {
        echo $data[0] + $data[1];


    }



}

?>
