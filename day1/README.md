# Day 1: Basics of PHP
- Basic declaration of PHP
- How to create a class
- How to create a function
- How to include a class file in a working file
- How to access a function in a working file
- How to pass values to a function
- How to return a value in a function
- How to declare an array in PHP
- How to pass values as array
- How to return a array from a function
- How to print an array in a working file

## Function Codes (class_lib.php)

### Basic echo function
```php
function helloworld()
{
    echo 'Hello World! Welcome to PHP Tutorial!';

}
```
### Basic function with params
```php
function basicadd($v1, $v2)
{
    echo $v1 + $v2;

}

```
### Basic function with return
```php
function addret($v1, $v2) //Function with return concept
{
    $result=$v1 + $v2;
    return $result; //Return the result

}
```
### Function with associative array as params   
```php
function medadd($data)
{
    echo $data['first'] + $data['second'];


}  
```
### Function with index array as params   
```php
function compadd($data) //Function with parameter as array Indexed
{
    echo $data[0] + $data[1];
}
```
## Try it on your own

Create a class file and a working file. Pass values to the function and do basic mathematical operations.
1. Pass two variables in a function and do multiplication of it
2. Pass 5 variables to a function as an array and add only the first and the last item and return the result
3. With the same array send it to another function and just add the second and the third number and return the value
4. Finally, get the result of 2 and 3 tasks and pass it to the task 1 function and print the result
