# Day 2: Array and Loop Concepts
- Array count
- For loop
- For each loop
- Sort: Basic Sort Indexed Array
- Sorting by Key
- Sorting by Values
- Reverse sorts
- if else conditions
- Switch loop

## Try it on your own

Create a class file and a working file.

1. Create an array for employees and their salary (10 employees)
2. Print the array by the employee name both Ascending and Descending separately
3. Print the array by the salary lowest to highest and highest to lowest
4. Create 5 salary levels based on the salary range and use switch case to display their level
