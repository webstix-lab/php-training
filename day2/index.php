<?php

require "class_lib.php";
$ln = new learn(); //Intialization of class

    ?>
<html>
<head>
  <title>Day 2: Array and Loop Concepts</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
  <div class="container">
<h1>Day 2: Array and Loop Concepts</h1>
<h6>By Ram | git@gitlab.com:webstix-lab/php-training.git</h6>
<hr/>
  <div class="starter-template">
    <?php //Your working file
    echo '<h4>Processing the array in for loop</h4>';
    $cars = array("Honda City", "Tata Altorz", "Bugatti Vernon", "Alfha Romea");
    $ln->processarray_for($cars);
    echo '<hr/>';

    echo '<h4>Processing the array in for-each loop</h4>';
    $ages = array("John"=>"35", "Jane"=>"30", "David"=>"12", "Nancy"=>"8");
    $ln->processarray_foreach($ages);
    echo '<hr/>';

    echo '<h4>Sort: Basic Sort Indexed Array</h4>';
    echo '<h5>Before Sorting</h5>';
    echo '<pre>';
    print_r($cars);
    echo '</pre>';

    echo '<h5>After Sorting: Ascending Order</h5>';
    echo '<code>sort($cars);</code>';
    echo '<pre>';
    sort($cars); // Basic
    print_r($cars);
    echo '</pre>';

    echo '<h5>After Sorting: Descending Order</h5>';
    echo '<code>rsort($cars);</code>';
    echo '<pre>';
    $cars = array("Honda City", "Tata Altorz", "Bugatti Vernon", "Alfha Romea");
    rsort($cars); // Basic Reverse
    print_r($cars);
    echo '</pre>';

    echo '<hr/>';
    echo '<h4>Sort: Basic Sort Associative Array</h4>';

    echo '<h5>Before Sorting</h5>';
    echo '<pre>';
    print_r($ages);
    echo '</pre>';

    echo '<h5>After Sorting by Key</h5>';
    echo '<code>ksort($ages);</code>';
    echo '<pre>';
    ksort($ages);
    print_r($ages);
    echo '</pre>';

    echo '<h5>After Sorting by Value</h5>';
    echo '<code>asort($ages);</code>';
    echo '<pre>';
    $ages = array("John"=>"35", "Jane"=>"30", "David"=>"12", "Nancy"=>"8");
    asort($ages);
    print_r($ages);
    echo '</pre>';

    echo '<h5>After Sorting by Value: Descending</h5>';
    echo '<code>arsort($ages);</code>';
    echo '<pre>';
    $ages = array("John"=>"35", "Jane"=>"30", "David"=>"12", "Nancy"=>"8");
    arsort($ages);
    print_r($ages);
    echo '</pre>';
    echo '<hr/>';
    echo '<h4>Basic If else</h4>';
    $yearold=22;
    if($yearold<18) {
        echo 'Master or Baby';
    }
    else {
        echo 'Adult';
    }
    echo '<hr/>';
    echo '<h4>Basic If elseif and else</h4>';
    $agetype='Baby';
    if($agetype=='Baby') {
        echo 'Age range 0 to 3';
    }
    else if($agetype=='Boy') {
          echo 'Age range 5 to 18';
    }
    else {
        echo 'Age after 18';
    }

    echo '<hr/>';
    echo '<h4>Switch Case:</h4>';

    $agecat = 'Teenager';

    switch ($agecat) {
    case "New Born":
        echo "Age is less than a day.<br/>";
        break;
    case "Infant":
        echo "Age is less than a year.<br/>";
        break;
    case "Baby":
        echo "Age is between 1 to 3.<br/>";
        break;
    case "Todller":
        echo "Age is between 4 to 7.<br/>";
        break;
    case "Boy":
        echo "Age is between 8 to 12.<br/>";
        break;
    case "Teenager":
        echo "Age is between 13 to 18.<br/>";
        break;
    case "Adult":
        echo "Age is more than 18.<br/>";
        break;
    default:
        echo "Age cat provided do not fall in any categorey";
    }

    ?>
</div>
</div>
</body>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html>
