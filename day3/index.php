<?php

require "class_lib.php";
$ln = new learn(); //Intialization of class

function printastable($data) //Print table
{
    $lt = new learn();
    echo '<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Student Name</th>
      <th scope="col">RollNo</th>
      <th scope="col">Class & Sec</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>';
    $j=1;
    for($a=0; $a<count($data); $a++)
    {
        $status=$lt->statusinfo($data[$a]['Status']);//Getting the status info
        $sclass=$lt->stucls($data[$a]['ID']); //Getting the student class info
        $ssec=$lt->stusec($data[$a]['ID']);

         echo '<tr>
          <th>'.$j.'</th>
          <td>'.$data[$a]['FirstName'].'</td>
          <td>'.$data[$a]['RollNo'].'</td>
          <td>'.$sclass['Name'].' & '.$ssec['SecName'].'</td>
          <td>'.$status.'</td>
        </tr>';
        $j++;

    }

    echo '</tbody>
</table>';

}
    ?>
<html>
<head>
  <title>Day 3: DB Connection and Query Operations</title>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link href="/php-training/fontawesome/css/all.css" rel="stylesheet">
</head>
<body>
  <div class="container">
<h1><i class="fas fa-calendar-day"></i> Day 3: DB Connection and Query Operations</h1>
<h6>By Ram | git@gitlab.com:webstix-lab/php-training.git</h6>
<hr/>
   <div class="starter-template">
       <h4>Calling table function with all status</h4>

    <?php
    $all = $ln->getallstudents();// Calling all student function
    printastable($all); //print the table

    echo '<hr/>';
    echo '<h4>Calling Same table function with only Active</h4>';

    $type='A';
    $students = $ln->getstudents($type);
    printastable($students); //print the table

    ?>

    <div class="row">
      <div class="col-4">
        <h4>Showing the use of GROUP BY</h4>
        <ul class="list-group">
        <?php
          $stucntinfo=$ln->getallstucount();
        for($si=0; $si<count($stucntinfo); $si++)
        {
            $cn=$ln->getclassinfo($stucntinfo[$si]['ClassID']);
            $classname=$cn['Name'];
             echo '<li class="list-group-item d-flex justify-content-between align-items-center">
              '.$classname.'
              <span class="badge badge-primary badge-pill">'.$stucntinfo[$si]['stucount'].'</span>
              </li>';
        }
          //print_r($stucntinfo);
            ?>
        </ul>
      </div>
      <div class="col-8"></div>
    </div>




</div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</html>
