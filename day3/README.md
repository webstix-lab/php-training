# Day 3: DB Connection and Query Operations
- Basic connection
- Fetch (fetch_assoc)
- Select single and multiple item
- WHERE & AND
- num_rows concepts
- ORDER BY
- GROUP BY
- while loop
- Return values as array
- Close the connection
- Sub queries
- How to process the DB result data in the working file

## Try it on your own

Create a class file and a working file.

1. Create a table with these info
  1. Table Name: Employee: ID, EmpCode, Name, Email, Phone, Designation, Address, Status, Salary, UpdatedOn
  2. Table Name: EmpSkills: ID, EmpID, Skill, YearsExp, PercentScore
  3. Table name: PayGrade: ID, StartRange, EndRange, Levels
2. Connect with the table and get all employee sorted by Ascending by the name
3. Write another function to show all employees in this format:

*John Doe*
_Sr. Programmer_

JS, PHP, CSS, MySQL

john@doe.com

603-455-7849

Pay Grade: *Level 5*


## DB Connection (db-config.php)

### Basic echo function
```php
$servername = "localhost";
$username = "username";
$password = "password";
$dbname = "DB Name";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
```
